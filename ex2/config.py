from chemtrayzer.ctydata.moldata import LevelOfTheory, QCMethod, ElectronicStructureProperties
from chemtrayzer.ctydata.basissets import basis_sets
from chemtrayzer.extcomp.gaussian import Gaussian

python_exec = 'source /home/ti102149/.zshrc\n'\
              'conda activate CTY\n'\
              'python'


program = Gaussian(executable='module load zlib/1.2.12 numactl/2.0.14 binutils/'
                                '2.38 Gaussian/16.C.01-AVX2\n'
                                'g16')

method = QCMethod.TPSS_D3
basis = basis_set=basis_sets['def2-SVP']
