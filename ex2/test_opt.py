'''
This file contains a test case for the Geometry Investigaiton that can be
executed by pytest. It checks if the correct Gaussian input file is created and
whether the "spe" field in the investigation's result is filled correctly.
'''
from pathlib import Path
from typing import ContextManager, Mapping

from chemtrayzer.ctydata.database import SpeciesDB
from chemtrayzer.ctydata.moldata import Geometry, QCMethod
from chemtrayzer.extcomp.gaussian import Gaussian
from chemtrayzer.investigations.investigations import InvestigationContext
from chemtrayzer.ctydata.basissets import basis_sets
from .opt import GeometryInvestigation
from chemtrayzer.util.testing import InvestigationTestCase

import pytest

_HERE = Path(__file__).parent.resolve()

class TestMyInvestigation(InvestigationTestCase[GeometryInvestigation]):
    JOB_INPUT_PATH = _HERE/'testfiles'/'expected_input'
    JOB_OUTPUT_PATH = _HERE/'testfiles'/'output'
    STEPS = ['submit_opt_job', 'read_spe']

    @pytest.fixture
    def context_managers(self, tmp_path) -> Mapping[str, ContextManager]:
        return {'species_db': SpeciesDB(tmp_path/'db.sqlite')}

    @pytest.fixture
    def investigation(self, inves_context: InvestigationContext)\
            -> GeometryInvestigation:
        geo = Geometry(['O', 'H', 'H'],
                        [[ 0.00000,   0.00000,   0.30000],
                         [-0.63500,   0.63500,  -0.33500],
                         [ 0.63500,  -0.63500,  -0.33500]])

        opts = GeometryInvestigation.Options(
            method=QCMethod.TPSS_D3,
            basis_set=basis_sets['def2-SVP'],
            program=Gaussian('g16')
        )

        return GeometryInvestigation(context=inves_context, geo=geo, opts=opts,
                                     charge=0, multiplicity=1)
    
    def step_1(self, inves: GeometryInvestigation):
        assert inves.result.spe == pytest.approx(-76.3607, rel=1e-6)