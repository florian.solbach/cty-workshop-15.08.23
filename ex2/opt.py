import logging
from pathlib import Path
from types import ModuleType
from typing import ContextManager, Iterable, Mapping
from chemtrayzer.util.cmdtools import CommandLineInterface
from dataclasses import dataclass
from chemtrayzer.investigations.investigations import Investigation, InvestigationContext, InvestigationState
from chemtrayzer.ctydata.moldata import Geometry, LevelOfTheory, ElectronicStructureProperties, QCMethod, BasisSet
from chemtrayzer.extcomp.gaussian import GaussianOptJob, Gaussian
from chemtrayzer.extcomp.jobsystem import JobState
import numpy as np

############################################################
# EDIT THIS CLASS
############################################################

class GeometryInvestigation(Investigation):
    '''optimizes a geometry with Gaussian'''

    @dataclass
    class Result(Investigation.Result):
        # define a field that stores the final single point energy
        ...

    @dataclass
    class Options:
        '''options/configuration/technical settings'''
        method: QCMethod = None
        '''quantum chemistry method for optimization'''
        basis_set: BasisSet = None
        '''basis set'''
        program: Gaussian = None
        '''instance of Gaussian program class with executable path'''

    def __init__(self, *, context: InvestigationContext, ...):
        super().__init__(context)

        # TODO store and set up the input and configuration 
        ...

        self.lot = LevelOfTheory(
            method=...,
            el_struc=ElectronicStructureProperties(
                basis_set=...,
                charge=..., multiplicity=...
            ))

        # TODO what needs to be done here?
        ...

    def submit_opt_job(self):
        # TODO submit a Gaussian optimization job
        ...

    def read_spe(self, job: GaussianOptJob):
        # TODO heck if the job was successful and read the SPE
        ...


############################################################
# IGNORE THE FOLLOWING (need to execute the investigation)
############################################################
class GeoCli(CommandLineInterface[GeometryInvestigation]):

    POSITIONAL_ARGUMENTS = [
        *CommandLineInterface.POSITIONAL_ARGUMENTS, # mandatory arguments
        ('geometry', '*.xyz file containing the initial geometry'),
        ('charge', 'total charge of the molecule'),
        ('multiplicity', 'spin multiplicity'),
    ]

    def get_context_managers(self, config: ModuleType,
                             cmd_args: Iterable[str])\
                             -> Mapping[str, ContextManager]:
        return {}

    def create_investigation(self, context: InvestigationContext,
                             config: ModuleType, cmd_args: Iterable[str])\
                            -> GeometryInvestigation:
        geo, _ = Geometry.from_xyz(cmd_args[1])
        charge = int(cmd_args[2])
        multiplicity = int(cmd_args[3])

        opts = GeometryInvestigation.Options(
            method=config.method,
            basis_set=config.basis,
            program=config.program
        )

        return GeometryInvestigation(context=context, geo=geo, opts=opts,
                                     charge=charge, multiplicity=multiplicity)

    def postprocessing(self, inves: GeometryInvestigation):
        if inves.state == InvestigationState.successful:
            logging.info('The final SPE is %.4f Hartree.', inves.result.spe)
        else:
            logging.info('The investigation failed. Reason: %s',
                         inves.result.reason)

if __name__ == '__main__':
    cli = GeoCli(Path(__file__))
    cli.start()
