import logging
from pathlib import Path
from types import ModuleType
from typing import ContextManager, Iterable, Mapping
from chemtrayzer.util.cmdtools import CommandLineInterface
from dataclasses import dataclass
from chemtrayzer.investigations.investigations import Investigation, InvestigationContext, InvestigationState
from chemtrayzer.ctydata.moldata import Geometry, LevelOfTheory, ElectronicStructureProperties, QCMethod, BasisSet
from chemtrayzer.ctydata.database import GeoId, SpeciesDB
from chemtrayzer.extcomp.gaussian import GaussianOptJob, Gaussian
from chemtrayzer.extcomp.jobsystem import JobState
import numpy as np

############################################################
# EDIT THIS CLASS
############################################################
class GeometryInvestigation(Investigation):
    '''optimizes a geometry with Gaussian'''

    # TODO declare a dependency on the database
    ...

    @dataclass
    class Result(Investigation.Result):
        spe: float = None
        '''single point energy of optimized geometry'''
        geo_id: GeoId = None
        '''id of the optimized geometry in the database'''

    @dataclass
    class Options:
        '''options/configuration/technical settings'''
        method: QCMethod = None
        '''quantum chemistry method for optimization'''
        basis_set: BasisSet = None
        '''basis set'''
        program: Gaussian = None
        '''instance of Gaussian program class with executable path'''

    def __init__(self, *,
                 geo: Geometry, context: InvestigationContext,
                 opts: Options, charge: int = 0, multiplicity: int = 1):
        super().__init__(context)

        self.geo = geo
        self.opts = opts
        self.charge = charge
        self.multiplicity = multiplicity

        self.lot = LevelOfTheory(
            method=opts.method,
            el_struc=ElectronicStructureProperties(
                basis_set=opts.basis_set,
                charge=charge, multiplicity=multiplicity
            ))

        self.add_step(self.submit_opt_job)

    def submit_opt_job(self):
        job = GaussianOptJob(geometry=self.geo,
                       lot=self.lot,
                       compute_frequencies=False,
                       program=self.opts.program)

        self.wait_for_and_submit(job)
        self.add_step(self.store_data)

    def store_data(self, job: GaussianOptJob):
        db: SpeciesDB = self.context['species_db']

        if job.state == JobState.successful:

            # TODO store the geometry in the database and remember its id
            ...
            self.result = ...

            self.state = InvestigationState.successful
        else:
            self.fail(job.result['reason'])

############################################################
# IGNORE THE FOLLOWING (need to execute the investigation)
############################################################
class GeoCli(CommandLineInterface[GeometryInvestigation]):

    POSITIONAL_ARGUMENTS = [
        *CommandLineInterface.POSITIONAL_ARGUMENTS, # mandatory arguments
        ('geometry', '*.xyz file containing the initial geometry'),
        ('charge', 'total charge of the molecule'),
        ('multiplicity', 'spin multiplicity'),
    ]

    def get_context_managers(self, config: ModuleType,
                             cmd_args: Iterable[str])\
                             -> Mapping[str, ContextManager]:
        return {'species_db': SpeciesDB(config.db)}

    def create_investigation(self, context: InvestigationContext,
                             config: ModuleType, cmd_args: Iterable[str])\
                            -> GeometryInvestigation:
        geo, _ = Geometry.from_xyz(cmd_args[1])
        charge = int(cmd_args[2])
        multiplicity = int(cmd_args[3])

        opts = GeometryInvestigation.Options(
            method=config.method,
            basis_set=config.basis,
            program=config.program
        )

        return GeometryInvestigation(context=context, geo=geo, opts=opts,
                                     charge=charge, multiplicity=multiplicity)


    def postprocessing(self, inves: GeometryInvestigation):
        if inves.state == InvestigationState.successful:
            logging.info('The final SPE is %.4f Hartree.', inves.result.spe)
            logging.info('Writing optimized geometry to out.xyz...')

            db: SpeciesDB = inves.context['species_db']
            geo = db.load_geometry(inves.result.geo_id)
            geo.to_xyz('out.xyz')

        else:
            logging.info('The investigation failed. Reason: %s',
                         inves.result.reason)

if __name__ == '__main__':
    cli = GeoCli(Path(__file__))
    cli.start()
