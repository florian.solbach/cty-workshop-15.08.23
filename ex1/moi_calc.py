import logging
from pathlib import Path
from types import ModuleType
from typing import ContextManager, Iterable, Mapping
from chemtrayzer.util.cmdtools import CommandLineInterface
from dataclasses import dataclass
from chemtrayzer.investigations.investigations import Investigation, InvestigationContext, InvestigationState
from chemtrayzer.ctydata.moldata import Geometry
import numpy as np

############################################################
# EDIT THIS CLASS
############################################################

class MoiInvestigation(Investigation):

    @dataclass
    class Result(Investigation.Result):
        eigv: np.ndarray = None
        '''principal moments of inertia'''

    # TODO take a geometry object as input
    def __init__(self, context: InvestigationContext, ...):
        super().__init__(context)

        # TODO store the input and add the next step
        ...

    # TODO define a step "compute_moi" that finishes the investigation
    ...

############################################################
# IGNORE THE FOLLOWING (need to execute the investigation)
############################################################

class MoiCli(CommandLineInterface[MoiInvestigation]):

    POSITIONAL_ARGUMENTS = [
        *CommandLineInterface.POSITIONAL_ARGUMENTS, # mandatory arguments
        ('geometry', '*.xyz file containing the initial geometry')
    ]

    def get_context_managers(self, config: ModuleType, cmd_args: Iterable[str]) -> Mapping[str, ContextManager]:
        return {}

    def create_investigation(self, context: InvestigationContext,
                             config: ModuleType, cmd_args: Iterable[str])\
                            -> MoiInvestigation:
        geo, _ = Geometry.from_xyz(cmd_args[1])

        return MoiInvestigation(context=context, geo=geo)

    def postprocessing(self, inves: MoiInvestigation):
        eigv_str = ', '.join([f"{ev:.1f}" for ev in inves.result.eigv])

        logging.info('The principal moments of inertia are %s a_0^2/amu.',
                     eigv_str)

if __name__ == '__main__':
    cli = MoiCli(Path(__file__))
    cli.start()
